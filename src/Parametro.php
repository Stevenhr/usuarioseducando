<?php

namespace educando\usuario;

use Illuminate\Database\Eloquent\Model;


class Parametro extends Model
{
    protected $table = 'tbl_parametros';
    protected $primaryKey= 'i_pk_id';
    protected $fillable = [
        'vc_parametro','i_estado',
    ];
    public $timestamps = true;
    
    public function __construct()
    {
        $this->connection = config('usuarios.conexionadm');
    } 
    public function detalles()
    {
    	return $this->hasMany(config('usuarios.modelo_darametrodetalles'), 'i_fk_id_parametro','i_pk_id');
    }
}