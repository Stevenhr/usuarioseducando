<?php

namespace educando\usuario\GestionPerfiles\Providers;

use Illuminate\Support\ServiceProvider;
use educando\usuario\GestionPerfiles\Repository\PerfilRepository;

class PerfilServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('educando\usuario\GestionPerfiles\Repository\PerfilInterface', function($app)
        {
            return new PerfilRepository;
        });
    }
}
