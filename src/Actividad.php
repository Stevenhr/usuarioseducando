<?php

namespace educando\usuario;

use Illuminate\Database\Eloquent\Model;


class Actividad extends Model
{
    
    protected $table = 'tbl_actividades';
    protected $primaryKey= 'i_pk_id';
    protected $fillable = ['vc_actividad', 'vc_descripcion','vc_redireccion','vc_imagen','i_estado','i_fk_id_modulo','i_fk_id_padre'];
    public $timestamps = true;

    public function modulo()
	{
		return $this->belongsTo(config('usuarios.modelo_modulo'), 'i_fk_id_modulo','i_pk_id');
	}

	public function tipos()
    {
        return $this->belongsToMany(config('usuarios.modelo_tipo'), 'tbl_tipo_actividad', 'actividad_id', 'tipo_id');
    }

    public function hijos()
    {
        return $this->hasMany(config('usuarios.modelo_actividad'), 'i_fk_id_padre','i_pk_id');
    }

    public function padre()
    {
        return $this->belongsTo(config('usuarios.modelo_actividad'), 'i_fk_id_padre','i_pk_id');
    }    

}