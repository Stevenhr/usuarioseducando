<?php

namespace educando\usuario;


class Estado
{
    const CODIGO_TIPO_DOCUMENTO = 1;
    const CODIGO_CIUDAD = 2;
    const CODIGO_ETNIA = 3;
    const CODIGO_EPS = 4;
    const CODIGO_GENERO = 5;
    const CODIGO_AREAS = 6;    
	const CODIGO_AREAS_PAZ_Y_SALVOS = 16;  
	const CODIGO_GRUPOS_DE_TRABAJO = 41;   
	const CODIGO_MODULO = 30;   
    const CODIGO_GRADOS = 13;
    const CODIGO_YEAR = 14;
    const CODIGO_TIPO_CLIENTE = 33;

}
