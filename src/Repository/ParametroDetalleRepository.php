<?php

namespace educando\usuario\Repository;
use educando\usuario\User;
use educando\usuario\ParametroDetalles as ParametroDetalle;
use Illuminate\Support\Facades\DB;
use educando\usuario\Estado;

class ParametroDetalleRepository
{

    static function tipoUsuario($tipo)
    {
        $estado = '';
        switch ($tipo) {
            case 1:
                $estado = 'SUPERVISOR';
            break;
            case 2:
                $estado = 'JEFE AREA';
            break;
            case 3:
                $estado = 'APOYO';
            break;
            default:
                $estado = 0;
            break;
        }

        return $estado;
    }


    static function tipoEstado($tipo)
    {
        $estado = '';
        switch ($tipo) {
            case 0:
                $estado = 'NO';
            break;
            case 1:
                $estado = 'SI';
            break;

            default:
                $estado = 0;
            break;
        }

        return $estado;
    }

    static function obtenerAnios(){
        $anios=array();
        for ($i=date("Y")-10; $i <= date("Y") ; $i++) { 
            $anios[$i]=$i;
        }
        return $anios;        
    }

    static function obtenerUsuarios(){
        return User::where('vc_estado','=',1)->get()->pluck('full_name', 'id')->toArray();
    }

    static function obtenerPorTipoUsuario($tipo){
        return User::with(['tiposPersona' => function($query) use ($tipo) {
              $query->where('tipo_id', $tipo);
            }])->get();
    }

    

    static function obtenerParametro($parametro){
        return ParametroDetalle::where('i_fk_id_parametro',$parametro) 
                ->where('i_estado',1)->pluck('vc_parametro_detalle', 'i_pk_id')->toArray();
    }

    static function obtenerParametroAll($parametro){
        return ParametroDetalle::where('i_fk_id_parametro',$parametro) 
                ->pluck('vc_parametro_detalle', 'i_pk_id')->toArray();
    }

    static function obtenerTablasConTriggers(){
        $tablas = DB::select('SELECT  DISTINCT T.event_object_table FROM INFORMATION_SCHEMA.TRIGGERS T');
        $arrayTablas = [];

        foreach ($tablas as $key => $value) {
            $arrayTablas[$value->event_object_table]=$value->event_object_table;
        }
        return $arrayTablas;
    }

    public function obtenerUser($area){ 
        return User::where('i_fk_area',$area)->with('area')->get();            
    }

    public function obtenerModulo($date)
    {
        
		$data = ParametroDetalle::where('i_fk_id_parametro',Estado::CODIGO_MODULO)
                                ->whereDate('tt_fecha','<=',$date)
                                ->whereDate('tt_fecha_fin','>=',$date)
                                ->first();
        return ($data) ? $data->i_pk_id : null;
	}
    
} 