<?php

namespace educando\usuario\Repository;

use educando\usuario\Repository\CRUDInterface;

interface UsuarioInterface extends CRUDInterface
{
	public function obtenerUsuarioPorId($id);
	public function obtenerUsuarioPorCedula($documento); 
	public function obtenerUsuariosPorArea($areas);
	public function obtenerUsuariosActivos();
}