<?php

namespace educando\usuario\Repository;
use educando\usuario\User;
use Auth; 
use Illuminate\Support\Facades\DB;
use educando\usuario\Repository\AuditoriaRepository as Aud;
use educando\usuario\Repository\UsuarioInterface;
class UsuarioRepository implements UsuarioInterface{

	public function obtenerUsuarioPorId($id){
		return User::find($id);		
	}

	public function obtenerUsuarioPorCedula($documento){
		return User::where('i_cedula',$documento)->first();		
	}

	public function obtenerUsuarioPorCedulas($documentos){
		return User::whereIn('i_cedula',$documentos)->get();		
	}

	public function obtenerUsuarioPorNombre($name){
		return DB::table('tbl_users')->where(DB::raw("CONCAT(name,' ',vc_segundo_nombre,' ',vc_primer_apellido,' ',vc_segundo_apellido)"), 'LIKE', '%' . $name . '%')->get();	
	}



	public function crear($request){
		//Para auditoría
		//Aud::setUserId('pgsql'); 
		$usuario = new User();
		$data = $request->only($usuario->getFillable());
		$data['vc_estado']=1; 
		$data['password'] = bcrypt($data['i_cedula']);
		if($usuario->fill($data)->save()){
			return $usuario->id;
		}else{
			return -1;
		}
	}

	public function actualizar($request,$id){
		//Para auditoría
		//Aud::setUserId('pgsql'); 		
		$usuario = User::find($id);
		$data = $request->only($usuario->getFillable());
		return $usuario->fill($data)->save();
	}

	public function obtenerUsuariosPorArea($areas){ 
		return User::whereIn('i_fk_area',$areas)->with('area')->get();  	 		
	}  

	public function obtenerUsuariosActivos(){
		return User::where('vc_estado',1)->get()->pluck('fullname','id')->toArray();
	}
	
	public function obtener($id, $relaciones = []){}
	public function eliminar($id){}
	public function obtenerTodo($relaciones = []){}
	public function dataTable($relaciones = []){}	

    public function resetPassword($id){        
        $usuario = User::find($id);
		$usuario->password = bcrypt($usuario->i_cedula);
		return $usuario->save();
    } 	

    public function obtenerUser($area){ 
		return User::where('i_fk_area',$area)->get();  	 		
	}

	public function obtenerPorTipoUsuario($tipo){
        $data = User::whereHas('tiposPersona', function($query) use ($tipo) {
			  		return $query->where('tipo_id', $tipo);
				})->get();

		return $data;
    }

    public function vincularTipoAcudiente($idUsuario, $idTipoPersona){
		$user = User::find($idUsuario);
		$user->tiposPersona()->attach($idTipoPersona,[
                'i_estado'=>1,
		]);
		
		return $user;
    }
}