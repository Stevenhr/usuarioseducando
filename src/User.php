<?php

namespace educando\usuario;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\Traits\EntrustUserTrait;



class User extends Authenticatable
{


    use Notifiable, SoftDeletes;
    use EntrustUserTrait {
        EntrustUserTrait::restore insteadof SoftDeletes;
    }

    public function __construct()
    {
        $this->connection = config('usuarios.conexionadm');
    } 
    
    protected $table = 'tbl_users';
    protected $primaryKey= 'id';
    protected $fillable = ['i_fk_tipo_documento',
     'i_cedula',
     'name',
     'vc_segundo_nombre',
    'vc_primer_apellido',
    'vc_segundo_apellido',
    'dt_fecha_nacimiento',
    'email',
    'i_fk_ciudad',
    'i_fk_genero',
    'i_fk_etnia',
    'i_fk_eps',
    'vc_telefono',
    'vc_celular',
    'vc_direccion',
    'password',
    'vc_estado',
    'i_fk_area',
    'vc_perfil_pic',
    'd_fecha_fin_contrato',
    'tx_firma',
    'i_fk_grupo',
     'i_fk_tipo_cliente' ,
     'i_fk_lugar'];
    protected $hidden = ['password', 'remember_token'];
    protected $dates = ['deleted_at'];
    protected $appends = ['full_name','FullApellidos'];
    

    public function tiposPersona()
    {
        return $this->belongsToMany(config('usuarios.modelo_tipo'), 'tbl_tipo_persona', 'user_id', 'tipo_id');
    }

    public function ciudad()
    {
        return $this->belongsTo(config('usuarios.modelo_darametrodetalles'), 'i_fk_ciudad','i_pk_id');
    }

    public function area()
    {
        return $this->belongsTo(config('usuarios.modelo_darametrodetalles'), 'i_fk_area','i_pk_id');
    }
    

    public function getFullNameAttribute()
    {
        return trim($this->name) . ' ' . trim($this->vc_segundo_nombre) . ' ' . trim($this->vc_primer_apellido). ' ' . trim($this->vc_segundo_apellido);
    }

    public function getFullApellidosAttribute()
    {
        return trim($this->vc_primer_apellido). ' ' . trim($this->vc_segundo_apellido). ' ' . trim($this->name) . ' ' . trim($this->vc_segundo_nombre);
    }
    public function getNameLastAttribute()
    {
        return trim($this->name). ' ' . trim($this->vc_primer_apellido);
    }

    public function tipoDocumento()
    {
        return $this->belongsTo(config('usuarios.modelo_darametrodetalles'), 'i_fk_tipo_documento','i_pk_id');
    }

}
