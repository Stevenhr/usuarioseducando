<?php

namespace educando\usuario;

use Illuminate\Database\Eloquent\Model;


class GrupoTrabajo extends Model
{
    
    protected $table = 'tbl_usuario_grupo';
    protected $primaryKey= 'i_pk_id';
    protected $fillable = ['i_fk_id_usuario', 'i_fk_id_grupo','d_fecha_retiro','i_estado'];
    public $timestamps = true;

    public function usuarios()
    {
        return $this->belongsTo(config('usuarios.modelo_user'), 'i_fk_id_usuario','id');
    }   

}