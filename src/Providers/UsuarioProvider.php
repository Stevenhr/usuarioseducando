<?php 

namespace educando\usuario\Providers;

use Illuminate\Support\ServiceProvider;
use educando\usuario\Repository\UsuarioInterface;
use educando\usuario\Repository\UsuarioRepository;

class UsuarioProvider extends ServiceProvider
{

	public function boot()

	{

	}

	public function register()
	{
        $this->app->bind(UsuarioInterface::class, function($app)
        {
            return new UsuarioRepository;
        });
	}

}
