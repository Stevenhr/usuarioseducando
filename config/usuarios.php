<?php

return array( 
 
  'conexion' => 'db_principal', 
  'conexionadm' => 'conex_usuarios',  
  'modulo' => '', 
  'seccion' => 'Personas', 
  'prefijo_ruta' => 'personas', 
  'prefijo_ruta_modulo' => 'actividad', 
 
  'modelo_user' => 'educando\usuario\User',
  'modelo_tipo' => 'educando\usuario\Tipo', 
  'modelo_darametrodetalles' => 'educando\usuario\ParametroDetalles', 
  'modelo_actividad' => 'educando\usuario\Actividad',  
  'modelo_modulo' => 'educando\usuario\Modulo',
  'modelo_grupo' => 'educando\usuario\GrupoTrabajo',
  
   
  //vistas que carga las vistas 
  'vista_lista' => 'list', 
 
  //lista 
  'lista'  => 'idrd.usuarios.lista', 
  'proyectos'=>'proyectos',
);